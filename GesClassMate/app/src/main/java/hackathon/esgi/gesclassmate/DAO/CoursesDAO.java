package hackathon.esgi.gesclassmate.DAO;

import java.util.List;

import hackathon.esgi.gesclassmate.Model.Courses;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public class CoursesDAO extends DAO<Courses>{

    @Override
    public List<Courses> findAll() {
        return null;
    }

    @Override
    public List<Courses> findById() {
        return null;
    }

    @Override
    public List<Courses> findByName() {
        return null;
    }

    @Override
    public Courses find(long id) {
        return null;
    }

    @Override
    public Courses create(Courses obj) {
        return null;
    }

    @Override
    public Courses update(Courses obj) {
        return null;
    }

    @Override
    public void delete(Courses obj) {

    }
}
