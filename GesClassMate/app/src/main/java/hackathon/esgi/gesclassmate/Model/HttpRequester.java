package hackathon.esgi.gesclassmate.Model;


import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import hackathon.esgi.gesclassmate.Model.Enum.HttpMethod;

public class HttpRequester extends AsyncTask<RequestParameters, Long, ResultApi> {

    private HTTPRequesterCallback context;
    private String requestId;

    public HttpRequester(HTTPRequesterCallback currentClass) {
        this.context = currentClass;
    }

    @Override
    protected ResultApi doInBackground(RequestParameters... params) {
        HttpURLConnection httpURLConnection = null;
        ResultApi content = new ResultApi();

        this.requestId = params[0].getRequestId();

        try {
            URL url = new URL(params[0].getUrl());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setReadTimeout(5000);
            httpURLConnection.setRequestMethod(params[0].getMethod().toString());
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            if(params[0].getMethod().toString().compareTo(HttpMethod.GET.toString()) != 0)
                httpURLConnection.setDoOutput(true);

            if(params[0].getToken().compareTo("") != 0)
                httpURLConnection.setRequestProperty("Authorization", params[0].getToken());

            if(params[0].getInputObject() != null) {
                String objectInString;

                objectInString = new Gson().toJson(params[0].getInputObject());

                objectInString = objectInString.replace("\\\"", "\"");

                if(objectInString.startsWith("\""))
                    objectInString = objectInString.substring(1);

                if(objectInString.endsWith("\""))
                    objectInString = objectInString.substring(0, objectInString.length() - 1);


                byte[] contentInBytes = objectInString.getBytes();

                httpURLConnection.setRequestProperty("Content-Length", Integer.toString(contentInBytes.length));

                OutputStream output = httpURLConnection.getOutputStream();
                output.write(contentInBytes);
            }

            content.code = httpURLConnection.getResponseCode();

            if(httpURLConnection.getResponseCode() >= 400) {
                content.result = convertStreamToString(httpURLConnection.getErrorStream());
            } else {
                InputStream is = httpURLConnection.getInputStream();

                if(httpURLConnection.getResponseCode() == 200 || httpURLConnection.getResponseCode() == 201)
                    content.result = convertStreamToString(is);

                is.close();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        httpURLConnection.disconnect();

        return content;
    }


    @Override
    protected void onPostExecute(ResultApi res) {
        this.context.callbackHttpRequest(res, this.requestId);
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(sb.length() > 0)
            sb.setLength(sb.length() - 1);

        return sb.toString();
    }
}
