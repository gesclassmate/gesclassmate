package hackathon.esgi.gesclassmate.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */

public class Question implements Serializable {

    private int idQuestion;
    private String questionText;
    private Boolean status;
    private Boolean goodAnswer;
    private ArrayList<Answers> answers;

    public Question(){

    }
    public int getIdQuestion(){
        return idQuestion;
    }

    public String getQuestionText(){
        return questionText;
    }

    public Boolean getStatus(){
        return status;
    }

    public Boolean getGoodAnswer(){
        return goodAnswer;
    }

    public Answers getAnswerById(int id){
        return answers.get(id);
    }
    public void setIdQuestion(int idQuestion){
        this.idQuestion = idQuestion;
    }

    public void setQuestionText(String questionText){
        this.questionText = questionText;
    }

    public void setStatus(Boolean status){
        this.status = status;
    }

    public void setGoodAnswer(Boolean goodAnswer){
        this.goodAnswer = goodAnswer;
    }

    public void setAnswersById(int id, Answers answer){
        this.answers.set(id, answer);
    }

    public void addAnswer(Answers answer){
        this.answers.add(answer);
    }

}
