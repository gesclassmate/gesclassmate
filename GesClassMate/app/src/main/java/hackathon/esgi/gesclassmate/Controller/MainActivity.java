package hackathon.esgi.gesclassmate.Controller;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Window;

import org.json.JSONException;
import org.json.JSONObject;

import hackathon.esgi.gesclassmate.Model.Enum.HttpMethod;
import hackathon.esgi.gesclassmate.Model.HTTPRequesterCallback;
import hackathon.esgi.gesclassmate.Model.HttpRequester;
import hackathon.esgi.gesclassmate.Model.RequestParameters;
import hackathon.esgi.gesclassmate.Model.ResultApi;
import hackathon.esgi.gesclassmate.R;


public class MainActivity extends AppCompatActivity implements HTTPRequesterCallback {
    public boolean isFirstStart;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                 fragmentTransaction.replace(R.id.content,new Home()).commit();
                    return true;
                case R.id.navigation_dashboard:

                    fragmentTransaction.replace(R.id.content,new Profile()).commit();

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        FragmentManager fragmentManagerb = getFragmentManager();
        FragmentTransaction transaction = fragmentManagerb.beginTransaction();
        transaction.replace(R.id.content,new Home()).commit();
        this.getToken("getToken");
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //  Intro App Initialize SharedPreferences
                SharedPreferences getSharedPreferences = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());

                //  Create a new boolean and preference and set it to true
                isFirstStart = getSharedPreferences.getBoolean("firstStart", true);

                //  Check either activity or app is open very first time or not and do action
                if (isFirstStart) {

                    //  Launch application introduction screen
                    Intent i = new Intent(MainActivity.this, MySlider.class);
                    startActivity(i);
                    SharedPreferences.Editor e = getSharedPreferences.edit();
                    e.putBoolean("firstStart", false);
                    e.apply();
                }
            }
        });
        t.start();

    }

    private void getToken(String requestId){
        String myToken = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "");

        JSONObject json = new JSONObject();
        try {
            json.put("token", myToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(myToken.compareTo("") != 0){
            new HttpRequester(this).execute(new RequestParameters(
                    this,
                    requestId,
                    "auth/token/",
                    HttpMethod.POST,
                    myToken,
                    json.toString()));
        } else {
            startActivityForResult(new Intent(this, Login.class), 1);
        }
    }

    @Override
    public void callbackHttpRequest(ResultApi result, String requestId) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            this.finish();
    }
}
