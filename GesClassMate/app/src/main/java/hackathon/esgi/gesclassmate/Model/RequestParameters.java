package hackathon.esgi.gesclassmate.Model;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import hackathon.esgi.gesclassmate.Model.Enum.HttpMethod;


public class RequestParameters {
    private String requestId;

    private String url;
    private HttpMethod method;

    private Object inputObject;

    private String token;

    public String getRequestId(){
        return requestId;
    }
    public String getUrl() {
        return url;
    }
    public HttpMethod getMethod() {
        return method;
    }
    public Object getInputObject(){
        return inputObject;
    }
    public String getToken() {
        return token;
    }


    public RequestParameters(
            Context context,
            String requestId,
            String url,
            HttpMethod method,
            String token) {
        this.requestId = requestId;
        this.url = buildUrl(context, url);
        this.method = method;
        this.token = token;

        this.inputObject = null;
    }

    public RequestParameters(
            Context context,
            String requestId,
            String url,
            HttpMethod method,
            String token,
            Object inputObject) {

        this(context, requestId, url, method, token);

        this.inputObject = inputObject;
    }


    private String buildUrl(Context context, String url) {
        //SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        return "http://10.33.2.13:3000/" + url;
    }
}
