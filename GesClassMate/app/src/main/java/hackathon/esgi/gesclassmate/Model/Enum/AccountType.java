package hackathon.esgi.gesclassmate.Model.Enum;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */

public enum AccountType{
    STUDENT,
    TEACHER;
}
