package hackathon.esgi.gesclassmate.DAO;

import java.util.List;

import hackathon.esgi.gesclassmate.Model.Question;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public class QuestionDAO extends DAO<Question> {
    @Override
    public List<Question> findAll() {
        return null;
    }

    @Override
    public List<Question> findById() {
        return null;
    }

    @Override
    public List<Question> findByName() {
        return null;
    }

    @Override
    public Question find(long id) {
        return null;
    }

    @Override
    public Question create(Question obj) {
        return null;
    }

    @Override
    public Question update(Question obj) {
        return null;
    }

    @Override
    public void delete(Question obj) {

    }
}
