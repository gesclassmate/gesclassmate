package hackathon.esgi.gesclassmate.DAO;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import hackathon.esgi.gesclassmate.Model.CryptTools;
import hackathon.esgi.gesclassmate.Model.Enum.HttpMethod;
import hackathon.esgi.gesclassmate.Model.HttpRequester;
import hackathon.esgi.gesclassmate.Model.RequestParameters;
import hackathon.esgi.gesclassmate.Model.User;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public class UserDAO extends DAO<User> {
    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public List<User> findById() {
        return null;
    }

    @Override
    public List<User> findByName() {
        return null;
    }

    @Override
    public User find(long id) {
    return null;
    }

    @Override
    public User create(User obj) {
        return null;
    }

    @Override
    public User update(User obj) {
        return null;
    }

    @Override
    public void delete(User obj) {

    }
}
