package hackathon.esgi.gesclassmate.Model;


public interface HTTPRequesterCallback {
    void callbackHttpRequest(ResultApi result, String requestId);
}
