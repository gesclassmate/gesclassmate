package hackathon.esgi.gesclassmate.Model;

import java.io.Serializable;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */


public class Multimedia implements Serializable {

    private int idMultimedia;
    private String type;
    private String source;

    public Multimedia(){

    }

    public int getIdMultimedia(){
        return this.idMultimedia;
    }

    public String getType(){
        return this.type;
    }

    public String getSource(){
        return this.source;
    }

    public void setIdMultimedia(int idMultimedia){
        this.idMultimedia = idMultimedia;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setSource(String source){
        this.source = source;
    }

}
