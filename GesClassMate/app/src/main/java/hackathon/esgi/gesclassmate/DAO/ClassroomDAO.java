package hackathon.esgi.gesclassmate.DAO;

import java.util.List;

import hackathon.esgi.gesclassmate.Model.Classroom;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public class ClassroomDAO extends DAO<Classroom>{

    @Override
    public List<Classroom> findAll() {
        return null;
    }

    @Override
    public List<Classroom> findById() {
        return null;
    }

    @Override
    public List<Classroom> findByName() {
        return null;
    }

    @Override
    public Classroom find(long id) {
        return null;
    }

    @Override
    public Classroom create(Classroom obj) {
        return null;
    }

    @Override
    public Classroom update(Classroom obj) {
        return null;
    }

    @Override
    public void delete(Classroom obj) {

    }
}
