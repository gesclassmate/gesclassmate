package hackathon.esgi.gesclassmate.DAO;

import java.sql.Connection;
import java.util.List;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public abstract class DAO<T> {


    //public Connection connect = ConnectionPostgreSQL.getInstance();

    /**
     * Permet de récupérer un objet via son ID
     * @param id
     * @return
     */

    public abstract List<T> findAll();

    public abstract List<T> findById();

    public abstract List<T> findByName();

    public abstract T find(long id);

    /**
     * Permet de créer une entrée dans la base de données
     * par rapport à un objet
     * @param obj
     */
    public abstract T create(T obj);

    /**
     * Permet de mettre à jour les données d'une entrée dans la base
     * @param obj
     */
    public abstract T update(T obj);

    /**
     * Permet la suppression d'une entrée de la base
     * @param obj
     */
    public abstract void delete(T obj);


}
