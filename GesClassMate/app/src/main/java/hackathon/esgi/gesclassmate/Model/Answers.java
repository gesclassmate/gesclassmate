package hackathon.esgi.gesclassmate.Model;

import java.io.Serializable;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */

public class Answers implements Serializable{

    private int idAnswer;
    private Boolean answer;
    private User user;

    public Answers(){

    }

    public int getIdAnswer(){
        return this.idAnswer;
    }

    public Boolean getAnswer(){
        return this.answer;
    }

    public User getUser(){
        return this.user;
    }

    public void setIdAnswer(int idAnswer){
        this.idAnswer = idAnswer;
    }

    public void setAnswer(Boolean answer){

        this.answer = answer;
    }

    public void setUser(User user){
        this.user = user;
    }
}
