package hackathon.esgi.gesclassmate.Controller;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import hackathon.esgi.gesclassmate.Service.MediaPlayerService;
import hackathon.esgi.gesclassmate.R;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */

public class AudioPlayerController  extends AppCompatActivity {

    private MediaPlayerService player;
    boolean serviceBound = false;

    Button btPlay;
    Button btPause;
    Button btChangePos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audioplayer);

        playAudio("http://www.memoclic.com/medias/sons-wav/2/728.mp3");
        btPlay = (Button) findViewById(R.id.bt_Play);
        btPause = (Button) findViewById(R.id.bt_pause);
        btChangePos = (Button) findViewById(R.id.bt_changePos);

        btPlay.setOnClickListener(clickPlay);
        btPause.setOnClickListener(clickPause);
        btChangePos.setOnClickListener(clickChangePos);

    }
    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;

            Toast.makeText(AudioPlayerController.this, "Service Bound", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    private void playAudio(String media) {
        //Check is service is active
        if (!serviceBound) {
            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            playerIntent.putExtra("media", media);
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            //Service is active
            //Send media with BroadcastReceiver
        }
    }

    private View.OnClickListener clickChangePos = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            player.seekTo(2000);
        }
    };
    private View.OnClickListener clickPlay = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            player.playMedia();
            //mp.setPlaybackParams(mp.getPlaybackParams().setSpeed(speed));

        }
    };

    private View.OnClickListener clickPause = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            player.pauseMedia();
        }
    };








}


