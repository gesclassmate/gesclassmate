package hackathon.esgi.gesclassmate.DAO;

import java.util.List;

import hackathon.esgi.gesclassmate.Model.Profile;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public class ProfileDAO extends DAO<Profile> {
    @Override
    public List<Profile> findAll() {
        return null;
    }

    @Override
    public List<Profile> findById() {
        return null;
    }

    @Override
    public List<Profile> findByName() {
        return null;
    }

    @Override
    public Profile find(long id) {
        return null;
    }

    @Override
    public Profile create(Profile obj) {
        return null;
    }

    @Override
    public Profile update(Profile obj) {
        return null;
    }

    @Override
    public void delete(Profile obj) {

    }
}
