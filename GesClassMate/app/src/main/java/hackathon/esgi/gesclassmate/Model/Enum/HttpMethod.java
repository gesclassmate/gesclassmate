package hackathon.esgi.gesclassmate.Model.Enum;

public enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
}