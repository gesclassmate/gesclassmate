package hackathon.esgi.gesclassmate.DAO;

import java.util.List;

import hackathon.esgi.gesclassmate.Model.Multimedia;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public class MultimediaDAO extends DAO<Multimedia> {
    @Override
    public List<Multimedia> findAll() {
        return null;
    }

    @Override
    public List<Multimedia> findById() {
        return null;
    }

    @Override
    public List<Multimedia> findByName() {
        return null;
    }

    @Override
    public Multimedia find(long id) {
        return null;
    }

    @Override
    public Multimedia create(Multimedia obj) {
        return null;
    }

    @Override
    public Multimedia update(Multimedia obj) {
        return null;
    }

    @Override
    public void delete(Multimedia obj) {

    }
}
