package hackathon.esgi.gesclassmate.Model.Adapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.Image;
import android.os.IBinder;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hackathon.esgi.gesclassmate.Controller.AudioPlayerController;
import hackathon.esgi.gesclassmate.Model.Courses;
import hackathon.esgi.gesclassmate.Model.Multimedia;
import hackathon.esgi.gesclassmate.R;
import hackathon.esgi.gesclassmate.Service.MediaPlayerService;

/**
 * Created by I-P34LOUNET on 06/10/2017.
 */

public class AudioAdapter  extends RecyclerView.Adapter<AudioAdapter.MyViewHolder>{
    private Context context;
    private List<Multimedia> multimediaList;

    private String mediaUrl;
    private ArrayList<Boolean> isPlayings;
    private ArrayList<Boolean> isPlayingFirstTime;
    private Boolean musicPlayed;
    private int positionPlayed;

    private MediaPlayerService player;
    boolean serviceBound = false;

    public void setMultimediaList(List<Multimedia> multimediaList) {
        this.multimediaList = multimediaList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView audioName;
        public ImageView bt_PlayPause;
        //public TextView date;

        public MyViewHolder(View view) {
            super(view);

            audioName = (TextView) view.findViewById(R.id.tv_audioName);
            bt_PlayPause = (ImageView) view.findViewById(R.id.bt_ppAudio);

           // date = (TextView) view.findViewById(R.id.txtDate);

        }
    }

    public AudioAdapter(Context context, List<Multimedia> multimediaList) {
        this.context = context;
        this.multimediaList = multimediaList;
        this.mediaUrl = "";
        this.isPlayings = new ArrayList<>();
        this.isPlayingFirstTime = new ArrayList<>();
        musicPlayed = false;
    }

    @Override
    public AudioAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.audio, parent, false);

        return new AudioAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(AudioAdapter.MyViewHolder holder, final int position) {
        final Multimedia multimedias = multimediaList.get(position);
        isPlayings.add(false);
        isPlayingFirstTime.add(false);
       // holder.room.setText("Salle : " + multimedias.getRoom());
        //holder.date.setText(multimedias.getDate().toString());
        final AudioAdapter.MyViewHolder holderFinal = holder;

        holder.audioName.setText("Audio : " + Integer.toString(multimedias.getIdMultimedia()));
        holder.bt_PlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPlayings.get(position))
                {
                    holderFinal.bt_PlayPause.setImageResource(R.drawable.ic_play_white_24dp);
                    player.pauseMedia();
                    isPlayings.set(position, false);
                } else {
                    if(isPlayingFirstTime.get(position))
                    {
                        if(musicPlayed){
                            player.mediaFile = "";
                            player.stopMedia();
                            //musicPlayed = false;
                        } else {
                            musicPlayed = true;

                        }
                        player.playMedia();
                    } else{


                        if(musicPlayed){
                            player.mediaFile = "";
                            player.stopMedia();
                            //musicPlayed = false;
                        } else {
                            musicPlayed = true;

                        }


                        playAudio(multimedias.getSource());
                        isPlayingFirstTime.set(position, true);
                        positionPlayed = position;
                    }

                    holderFinal.bt_PlayPause.setImageResource(R.drawable.pause);
                    isPlayings.set(position, true);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return multimediaList.size();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            //serviceBound = true;

           // Toast.makeText(AudioPlayerController.this, "Service Bound", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    private void playAudio(String media) {
        //Check is service is active
        if (!serviceBound) {
            Intent playerIntent = new Intent(this.context, MediaPlayerService.class);
            playerIntent.putExtra("media", media);
            this.context.startService(playerIntent);
            this.context.bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            //Service is active
            //Send media with BroadcastReceiver
        }
    }
}
