package hackathon.esgi.gesclassmate.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import hackathon.esgi.gesclassmate.Model.CryptTools;
import hackathon.esgi.gesclassmate.Model.Enum.HttpMethod;
import hackathon.esgi.gesclassmate.Model.HTTPRequesterCallback;
import hackathon.esgi.gesclassmate.Model.HttpRequester;
import hackathon.esgi.gesclassmate.Model.RequestParameters;
import hackathon.esgi.gesclassmate.Model.ResultApi;
import hackathon.esgi.gesclassmate.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Profile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile extends Fragment implements HTTPRequesterCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String id;
    private TextView tv_NomPrenom;
    private TextView tv_Mail;
    private TextView tv_School;
    private TextView tv_Points;

    private Button bt_deco;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Profile() {
        // Required empty public constructor
    }

    public Profile(String mParam1, String mParam2, OnFragmentInteractionListener mListener) {
        this.mParam1 = mParam1;
        this.mParam2 = mParam2;
        this.mListener = mListener;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Profile.
     */
    // TODO: Rename and change types and number of parameters
    public static Profile newInstance(String param1, String param2) {
        Profile fragment = new Profile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        tv_NomPrenom = (TextView) v.findViewById(R.id.tv_nomPrenom);
        tv_Mail = (TextView) v.findViewById(R.id.tv_mail);
        tv_School = (TextView) v.findViewById(R.id.tv_school);
        tv_Points = (TextView) v.findViewById(R.id.tv_point);
        bt_deco = (Button) v.findViewById(R.id.bt_deco);

        bt_deco.setOnClickListener(decoClick);
        getUserId();

        return v;
    }

    public View.OnClickListener decoClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            disconnect();

        }
    };

    public void disconnect() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).edit();
        editor.putString("token", "");
        editor.commit();
        startActivityForResult(new Intent(this.getActivity(), Login.class), 1);
        //this.onDetach();
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getProfile(){
        JSONObject json = new JSONObject();

        new HttpRequester(this).execute(
                new RequestParameters(
                        this.getActivity(),
                        "user",
                        "user/"+id,
                        HttpMethod.GET,
                        ""));
    }

    private void getUserId(){
        JSONObject json = new JSONObject();
        String myToken = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getString("token", "");

        new HttpRequester(this).execute(
                new RequestParameters(
                        this.getActivity(),
                        "id",
                        "user/token/"+myToken,
                        HttpMethod.GET,
                        ""));
    }

    @Override
    public void callbackHttpRequest(ResultApi result, String requestId) {

        if(requestId == "id")
        {
            JSONArray arr = null;
            try {
                arr = new JSONArray(result.getResult());

                JSONObject jObj = arr.getJSONObject(0);
                id = jObj.getString("idUser");

                getProfile();
            } catch (JSONException e) {
                e.printStackTrace();
            }



        } else {

            Log.d("USER", result.getResult());
            hackathon.esgi.gesclassmate.Model.Profile p = new hackathon.esgi.gesclassmate.Model.Profile();
/*JSONArray arr = new JSONArray(result);
JSONObject jObj = arr.getJSONObject(0);
String date = jObj.getString("NeededString");*/

            // p = new Gson().fromJson(result.getResult(), hackathon.esgi.gesclassmate.Model.Profile.class);
            try {
                JSONArray arr = new JSONArray(result.getResult());
                JSONObject jObj = arr.getJSONObject(0);
                p.setFirstName(jObj.getString("Firstname"));
                p.setLastName(jObj.getString("Lastname"));
                p.setSchool(jObj.getString("School"));
                p.setPoints(jObj.getInt("Points"));
                tv_NomPrenom.setText(p.getFirstName() + " " + p.getLastName());
                tv_Mail.setText(p.getFirstName()+p.getLastName()+"@myges.com");
                tv_School.setText(p.getSchool());
                tv_Points.setText(String.valueOf(p.getPoints()) + "points");

                //Log.d("USERCUT",firstname);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }



    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
