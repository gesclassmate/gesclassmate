package hackathon.esgi.gesclassmate.Model;

import java.io.Serializable;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */

public class Profile implements Serializable {

    private int idProfile;
    private String FirstName;
    private String LastName;
    private String School;
    private int Points;

    public Profile(){

    }

    public int getIdProfile(){
        return this.idProfile;
    }

    public String getFirstName(){
        return this.FirstName;
    }

    public String getLastName(){
        return this.LastName;
    }

    public String getSchool(){
        return this.School;
    }

    public int getPoints(){
        return this.Points;
    }

    public void setIdProfile(int idProfile){
        this.idProfile = idProfile;
    }

    public void setFirstName(String firstName){
        this.FirstName = firstName;
    }


    public void setLastName(String lastName) {
        this.LastName = lastName;
    }

    public void setSchool(String school){
        this.School = school;
    }

    public void setPoints(int points){
        this.Points = points;
    }

    public void addPoint(){
        this.Points += 1;
    }

    public void removePoint(){
        this.Points -= 1;
    }
}
