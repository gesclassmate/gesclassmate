package hackathon.esgi.gesclassmate.Controller;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import hackathon.esgi.gesclassmate.R;
import io.saeid.fabloading.LoadingView;

import static java.security.AccessController.getContext;

/**
 * Created by moncef on 06/10/17.
 */

public class SplashScreen extends AppCompatActivity {
    private LoadingView mLoadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splashscreen);


        mLoadingView = (LoadingView) findViewById(R.id.loading_view);
        mLoadingView.addAnimation(Color.parseColor("#FFFFFF"),R.drawable.ican,LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#FFFFFF"),R.drawable.esgi,LoadingView.FROM_BOTTOM);
        mLoadingView.addAnimation(Color.parseColor("#FFFFFF"),R.drawable.ppa,LoadingView.FROM_TOP);

        mLoadingView.startAnimation();

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override public void onAnimationStart(int currentItemPosition) {

            }

            @Override public void onAnimationRepeat(int nextItemPosition) {
            }

            @Override public void onAnimationEnd(int nextItemPosition) {

            }
        });
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                startActivity( new Intent(SplashScreen.this, MainActivity.class));
            }
        }, 5000);
    }

}





