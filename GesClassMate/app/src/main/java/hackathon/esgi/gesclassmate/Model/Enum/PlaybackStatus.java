package hackathon.esgi.gesclassmate.Model.Enum;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public enum PlaybackStatus {
    PLAYING,
    PAUSED
}
