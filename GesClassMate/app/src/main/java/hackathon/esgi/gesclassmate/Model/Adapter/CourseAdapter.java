package hackathon.esgi.gesclassmate.Model.Adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import hackathon.esgi.gesclassmate.Model.Courses;
import hackathon.esgi.gesclassmate.R;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.MyViewHolder>{
    private Context context;
    private List<Courses> courseList;

    public void setCourseList(List<Courses> courseList) {
        this.courseList = courseList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView room;
        public TextView date;

        public MyViewHolder(View view) {
            super(view);

            room = (TextView) view.findViewById(R.id.txtSalle);
            date = (TextView) view.findViewById(R.id.txtDate);


        }
    }

    public CourseAdapter(Context context, List<Courses> courseList) {
        this.context = context;
        this.courseList = courseList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.course, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Courses courses = courseList.get(position);

        holder.room.setText("Salle : " + courses.getRoom());
        holder.date.setText(new SimpleDateFormat("dd-MM-yyyy").format(courses.getDate()));
    }

    @Override
    public int getItemCount() {
        return courseList.size();
    }
}
