package hackathon.esgi.gesclassmate.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */

public class Classroom implements Serializable {

    private int idClassroom;
    private String className;
    private ArrayList<Profile> profiles;
    private ArrayList<Courses> courses;

    public Classroom(){

    }

    public int getIdClassroom(){
        return idClassroom;
    }

    public String getClassName(){
        return className;
    }

    public Profile getProfileById(int id){
        return profiles.get(id);
    }

    public Courses getCoursesById(int id){
        return courses.get(id);
    }

    public void setIdClassroom(int idClassroom){
        this.idClassroom = idClassroom;
    }

    public void setClassName(String className){
        this.className = className;
    }

    public void setProfileById(int id, Profile profile){
        this.profiles.set(id, profile);
    }

    public void setCoursesById(int id, Courses courses){
        this.courses.set(id, courses);
    }

    public void addCourse(Courses course){
        this.courses.add(course);
    }

    public void addProfile(Profile profile){
        this.profiles.add(profile);
    }
}
