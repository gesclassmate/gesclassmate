package hackathon.esgi.gesclassmate.Model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */

public class Courses implements Serializable {

    private int idCourse;
    private String subject;
    private String room;
    private Date date;
    private ArrayList<Multimedia> multimedias;
    private ArrayList<Question> questions;

    public Courses(){

    }

    public Courses(int idCourse, String subject, String room) {
        this.idCourse = idCourse;
        this.subject = subject;
        this.room = room;
    }

    public Courses(int idCourse, String subject, String room, Date date) {
        this.idCourse = idCourse;
        this.subject = subject;
        this.room = room;
        this.date = date;
    }

    public int getIdCourse(){
        return idCourse;
    }

    public String getSubject(){
        return subject;
    }

    public String getRoom(){
        return room;
    }

    public Date getDate(){
        return date;
    }

    public Multimedia getMultimediaById(int id){
        return this.multimedias.get(id);
    }

    public Question getQuestionById(int id){
        return this.questions.get(id);
    }
    public void setIdCourse(int idCourse){
        this.idCourse = idCourse;
    }

    public void setSubject(String subject){
        this.subject = subject;
    }
    public void setRoom(String room){
        this.room = room;
    }
    public void setDate(Date date){
        this.date = date;
    }

    public void setMultimediaById(int id, Multimedia multimedia){
        this.multimedias.set(id, multimedia);
    }

    public void setQuestions(int id, Question question){
        this.questions.set(id, question);
    }

    public void addMultimedia(Multimedia multimedia){
        this.multimedias.add(multimedia);
    }

    public void addQuestion(Question question){
        this.questions.add(question);
    }
}
