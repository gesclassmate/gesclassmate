package hackathon.esgi.gesclassmate.DAO;

import java.util.List;

import hackathon.esgi.gesclassmate.Model.Answers;

/**
 * Created by I-P34LOUNET on 05/10/2017.
 */

public class AnswersDAO extends  DAO<Answers>{

    @Override
    public List<Answers> findAll() {
        return null;
    }

    @Override
    public List<Answers> findById() {
        return null;
    }

    @Override
    public List<Answers> findByName() {
        return null;
    }

    @Override
    public Answers find(long id) {
        return null;
    }

    @Override
    public Answers create(Answers obj) {
        return null;
    }

    @Override
    public Answers update(Answers obj) {
        return null;
    }

    @Override
    public void delete(Answers obj) {

    }
}
