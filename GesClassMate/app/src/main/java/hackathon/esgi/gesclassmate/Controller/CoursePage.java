package hackathon.esgi.gesclassmate.Controller;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hackathon.esgi.gesclassmate.Model.Adapter.AudioAdapter;
import hackathon.esgi.gesclassmate.Model.Adapter.CourseAdapter;
import hackathon.esgi.gesclassmate.Model.ClickListener;
import hackathon.esgi.gesclassmate.Model.Courses;
import hackathon.esgi.gesclassmate.Model.Enum.HttpMethod;
import hackathon.esgi.gesclassmate.Model.HTTPRequesterCallback;
import hackathon.esgi.gesclassmate.Model.HttpRequester;
import hackathon.esgi.gesclassmate.Model.Multimedia;
import hackathon.esgi.gesclassmate.Model.RecyclerTouchListener;
import hackathon.esgi.gesclassmate.Model.RequestParameters;
import hackathon.esgi.gesclassmate.Model.ResultApi;
import hackathon.esgi.gesclassmate.R;


public class CoursePage extends Fragment implements HTTPRequesterCallback {

    static Bundle args = new Bundle();

    RecyclerView recyclerView;
    List<Multimedia> multimediaList;
    AudioAdapter audioAdapter;
    View v;

    private TextView tv_salleX;

    static CoursePage fragment;
    public CoursePage() {
        // Required empty public constructor
    }

    public static CoursePage newInstance() {
        fragment = new CoursePage();
        //Bundle args = new Bundle();
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.coursepage, container, false);
        tv_salleX = (TextView) v.findViewById(R.id.tv_salleX);

        tv_salleX.setText(fragment.getArguments().getString("room"));
        //getCourse();
        getMultimedia();
        return v;
    }

    private void getMultimedia(){

        JSONObject json = new JSONObject();
        try {
            json.put("token", PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getString("token", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }



        new HttpRequester(this).execute(
                new RequestParameters(
                        this.getActivity(),
                        "getMultimedia",
                        "media/byCourse/"+String.valueOf(fragment.getArguments().getInt("idCourse")),
                        HttpMethod.GET,
                        ""));
    }

    @Override
    public void callbackHttpRequest(ResultApi result, String requestId) {
        if(requestId.compareTo("getMultimedia") == 0) {
            Log.d(requestId, result.getResult());

            multimediaList = new ArrayList<>();

            try {
                if(!result.getResult().isEmpty())
                {
                    JSONArray arr = new JSONArray(result.getResult());
                    Log.d("JSONARRAY", arr.toString());
                    for(int i = 0; i < arr.length(); i++){
                        JSONObject jObj = arr.getJSONObject(i);


                        Multimedia m = new Multimedia();
                        m.setSource(jObj.getString("Source"));
                        m.setIdMultimedia(jObj.getInt("idMultimedia"));
                        multimediaList.add(m);

                        Log.d("SOURCEZ", m.getSource());


                    }

                    if(arr.length() != 0)
                    {
                        initList();
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }



        }
    }

    private void initList(){
        recyclerView = (RecyclerView) v.findViewById(R.id.audio_recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this.getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                /*Bundle bundle = new Bundle();
                bundle.putInt("idCourse", multimediaList.get(position).getIdCourse());
                bundle.putString("room", multimediaList.get(position).getRoom());

                Fragment fragment = CoursePage.newInstance();
                fragment.setArguments(bundle);

                FragmentManager fm = getFragmentManager();

                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.content, fragment);
                fragmentTransaction.commit();*/
            }

            @Override
            public void onLongClick(View view, int position) {
                onClick(view, position);
            }
        }));
        audioAdapter = new AudioAdapter(this.getActivity(), multimediaList);
        recyclerView.setAdapter(audioAdapter);
        // recyclerView.refreshDrawableState();

    }
}
