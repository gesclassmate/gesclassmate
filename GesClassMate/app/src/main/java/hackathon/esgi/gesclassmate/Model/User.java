package hackathon.esgi.gesclassmate.Model;

import java.io.Serializable;

import hackathon.esgi.gesclassmate.Model.Enum.AccountType;

/**
 * Created by I-P34LOUNET on 04/10/2017.
 */

public class User implements Serializable {

    private int idUser;
    private String email;
    private String username;
    private String password;
    private AccountType accountType;
    private Profile profile;

    public User(){

    }

    public int getIdUser(){
        return this.idUser;
    }

    public String getEmail(){
        return this.email;
    }

    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    public AccountType getAccountType(){
        return this.accountType;
    }

    public Profile getProfile(){
        return this.profile;
    }
    public void setIdUser(int idUser){
        this.idUser = idUser;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setAccountType(AccountType accountType){
        this.accountType = accountType;
    }

    public void setProfile(Profile profile){
        this.profile = profile;
    }

}
