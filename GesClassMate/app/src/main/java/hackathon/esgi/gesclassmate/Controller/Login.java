package hackathon.esgi.gesclassmate.Controller;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import hackathon.esgi.gesclassmate.Model.CryptTools;
import hackathon.esgi.gesclassmate.Model.Enum.HttpMethod;
import hackathon.esgi.gesclassmate.Model.HTTPRequesterCallback;
import hackathon.esgi.gesclassmate.Model.HttpRequester;
import hackathon.esgi.gesclassmate.Model.RequestParameters;
import hackathon.esgi.gesclassmate.Model.ResultApi;
import hackathon.esgi.gesclassmate.R;

public class Login extends AppCompatActivity implements HTTPRequesterCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        initUI();
    }

    private void initUI() {
        findViewById(R.id.buttonConnect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect(((EditText)findViewById(R.id.username)).getText().toString(),
                        ((EditText)findViewById(R.id.password)).getText().toString());
            }
        });
    }

    private void connect(String username, String password){
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            json.put("password", CryptTools.SHA1(password));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new HttpRequester(this).execute(
                new RequestParameters(
                        this,
                        "connect",
                        "auth/signin",
                        HttpMethod.POST,
                        "",
                        json.toString()));
    }

    @Override
    public void callbackHttpRequest(ResultApi result, String requestId) {

        if(requestId.compareTo("connect") == 0){
            if(result.getCode() >= 400){

                new AlertDialog.Builder(this)
                        .setTitle("Error")
                        .setMessage(result.getResult())
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();

            } else {
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
                editor.putString("token", result.getResult());
                editor.commit();

                setResult(RESULT_OK);

                this.finish();
            }
        }

    }
}
