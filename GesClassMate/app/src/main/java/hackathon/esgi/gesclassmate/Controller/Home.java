package hackathon.esgi.gesclassmate.Controller;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hackathon.esgi.gesclassmate.Model.Adapter.CourseAdapter;
import hackathon.esgi.gesclassmate.Model.ClickListener;
import hackathon.esgi.gesclassmate.Model.Courses;
import hackathon.esgi.gesclassmate.Model.CryptTools;
import hackathon.esgi.gesclassmate.Model.Enum.HttpMethod;
import hackathon.esgi.gesclassmate.Model.HttpRequester;
import hackathon.esgi.gesclassmate.Model.HTTPRequesterCallback;
import hackathon.esgi.gesclassmate.Model.RecyclerTouchListener;
import hackathon.esgi.gesclassmate.Model.RequestParameters;
import hackathon.esgi.gesclassmate.Model.ResultApi;
import hackathon.esgi.gesclassmate.R;


public class Home extends Fragment implements HTTPRequesterCallback {

    RecyclerView recyclerView;
    List<Courses> coursesList;
    CourseAdapter courseAdapter;
    View view;

    Boolean firstTime = true;

    public Home() {
        // Required empty public constructor
    }

    public static Home newInstance() {
        Home fragment = new Home();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_home, container, false);

        getCourse();

        return view;
    }

    private void getCourse(){
        JSONObject json = new JSONObject();
        try {
            json.put("token", PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getString("token", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new HttpRequester(this).execute(
                new RequestParameters(
                        this.getActivity(),
                        "getCourse",
                        "course/mycourses",
                        HttpMethod.POST,
                        PreferenceManager
                                .getDefaultSharedPreferences(this.getActivity()).getString("token", ""),
                        json.toString()));
    }

    @Override
    public void callbackHttpRequest(ResultApi result, String requestId) {
        if(requestId.compareTo("getCourse") == 0) {
            Log.d(requestId, result.getResult());

            coursesList = new ArrayList<>();

            try {
                JSONArray arr = new JSONArray(result.getResult());

                for(int i = 0; i < arr.length(); i++){
                    JSONObject jObj = arr.getJSONObject(i);

                    Date date = null;

                    try {
                        date = new SimpleDateFormat("yyyy-MM-dd").parse(jObj.getString("Date"));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    coursesList.add(
                            new Courses(
                                    jObj.getInt("idCourse"),
                                    jObj.getString("Subject"),
                                    jObj.getString("Room"),
                                    date));

                    Log.d(Integer.toString(i), Integer.toString(jObj.getInt("idCourse")) + ", " +
                                    jObj.getString("Subject") + ", " + jObj.getString("Room"));


                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            initList();
        }
    }

    private void initList(){
        recyclerView = (RecyclerView) view.findViewById(R.id.course_recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this.getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Bundle bundle = new Bundle();
               bundle.putInt("idCourse", coursesList.get(position).getIdCourse());
                bundle.putString("room", coursesList.get(position).getRoom());

                Fragment fragment = CoursePage.newInstance();
                fragment.setArguments(bundle);

                FragmentManager fm = getFragmentManager();

                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.content, fragment);
                fragmentTransaction.commit();
            }

            @Override
            public void onLongClick(View view, int position) {
                onClick(view, position);
            }
        }));
        courseAdapter = new CourseAdapter(this.getActivity(), coursesList);
        recyclerView.setAdapter(courseAdapter);
       // recyclerView.refreshDrawableState();

    }
}
